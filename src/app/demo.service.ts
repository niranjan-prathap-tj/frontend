import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Message } from 'src/message.model';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DemoService {
private demoUrl!:string;
  constructor(private http:HttpClient) { 
    this.demoUrl = 'http://localhost:8080/';
  }
  messages!:Message[];
  getGeneralMessages():Observable<any>{
    return this.http.get(this.demoUrl + 'api/v1');
  }
  getWishes():Observable<any> {
    return this.http.get(this.demoUrl + 'api/v2');
  }
  
  getHealthStatus():Observable<any> {
    return this.http.get(this.demoUrl + 'actuator/health'); 
  }

}
